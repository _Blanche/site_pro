@echo off
SETLOCAL
TITLE %~n0

if [%1] == [] (
    echo:
    echo A name as the 1st argument is needed to create the project
    echo Exiting batch now
    TITLE %comspec%
    ENDLOCAL
    EXIT /b
)

SET _prjctname=%1

if EXIST ".\%_prjctname%" (
    echo:
    echo Choose another name, this one's already taken
    echo Exiting batch now
) else (
    echo:
    echo ===== Those lines are normal: npm WARN optional SKIPPING OPTIONAL DEPENDENCY

    CALL :CreateVue2Project
    CALL :InstallBootstrapVue
    CALL :CleanDependencies
    CALL :InstallVueFontAwesome
    CALL :RewriteViteConfigJs
    CALL :RewriteMainJs
    CALL :RewriteAppVue
    CALL :CreateHeaderVue
    CALL :CreateFooterVue

    del .\src\components\HelloWorld.vue

    echo %~dp0%_prjctname%^>move .\node_modules\.bin\vite.cmd .\
    move .\node_modules\.bin\vite.cmd .\
    CALL :RewriteViteCmd

    echo:
    echo =====
    echo Tadaaa...
    echo Use 'cd %_prjctname%' then 'vite' and start coding
)

TITLE %comspec%
ENDLOCAL
EXIT /b

:CreateVue2Project
    echo:
    echo ===== Creating Vue 2 project
    echo:
    echo on
    CALL npm init @vitejs/app %_prjctname% --template vue | EXIT
    cd %_prjctname%
    CALL npm install | EXIT
    CALL npm install vite-plugin-vue2 --dev | EXIT
    CALL npm install vue-template-compiler | EXIT
    CALL npm install vue@2 | EXIT
    @echo off
EXIT /b

:InstallBootstrapVue
    echo:
    echo ===== Installing BootstrapVue
    echo:
    echo on
    CALL npm install bootstrap@">=4.5.3 <5.0.0" | EXIT
    CALL npm install jquery | EXIT
    CALL npm install bootstrap-vue | EXIT
    @echo off
EXIT /b

:CleanDependencies
    echo:
    echo ===== Removing Vue 3's thingies
    echo:
    echo on
    CALL npm uninstall @vue/compiler-sfc | EXIT
    CALL npm uninstall @vitejs/plugin-vue | EXIT
    @echo off
EXIT /b

:InstallVueFontAwesome
    echo:
    echo ===== Installing VueFontAwesome
    echo:
    echo on
    CALL npm install @fortawesome/fontawesome-svg-core | EXIT
    CALL npm install @fortawesome/vue-fontawesome@latest | EXIT
    CALL npm install @fortawesome/free-solid-svg-icons | EXIT
    CALL npm install @fortawesome/free-regular-svg-icons | EXIT
    CALL npm install @fortawesome/free-brands-svg-icons | EXIT
    @echo off
EXIT /b

:RewriteViteCmd
    echo:
    echo ===== Rewriting .\vite.cmd
    echo:
    echo ^@ECHO off>vite.cmd
    echo SETLOCAL>>vite.cmd
    echo CALL ^:find_dp0>>vite.cmd
    echo:>>vite.cmd
    echo IF EXIST "%%dp0%%\node.exe" ^(>>vite.cmd
    echo    SET "_prog=%%dp0%%\node.exe">>vite.cmd
    echo ^) ELSE ^(>>vite.cmd
    echo    SET "_prog=node">>vite.cmd
    echo    SET PATHEXT=%%PATHEXT^:;.JS;=;%%>>vite.cmd
    echo ^)>>vite.cmd
    echo:>>vite.cmd
    echo "%%_prog%%"  "%%dp0%%\.\node_modules\vite\bin\vite.js" %%*>>vite.cmd
    echo ENDLOCAL>>vite.cmd
    echo EXIT /b %%errorlevel%%>>vite.cmd
    echo ^:find_dp0>>vite.cmd
    echo SET dp0=%%^~dp0>>vite.cmd
    echo EXIT /b>>vite.cmd
EXIT /b

:RewriteViteConfigJs
    echo:
    echo ===== Rewriting .\vite.config.js
    echo:
    echo const { createVuePlugin } = require^('vite-plugin-vue2'^);>vite.config.js
    echo:>>vite.config.js
    echo module.exports = {>>vite.config.js
    echo    plugins: [createVuePlugin^(^)]>>vite.config.js
    echo };>>vite.config.js
EXIT /b

:RewriteMainJs
    echo:
    echo ===== Rewriting .\src\main.js
    echo:
    echo import Vue from 'vue';>.\src\main.js
    echo import App from './App.vue';>>.\src\main.js
    echo import { BootstrapVue, BootstrapVueIcons, BIconEnvelope, BIconTelephone } from 'bootstrap-vue';>>.\src\main.js
    echo import { library } from '@fortawesome/fontawesome-svg-core';>>.\src\main.js
    echo import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome';>>.\src\main.js
    echo import { faThumbsUp } from '@fortawesome/free-regular-svg-icons';>>.\src\main.js
    echo:>>.\src\main.js
    echo import 'bootstrap/dist/css/bootstrap.css';>>.\src\main.js
    echo import 'bootstrap-vue/dist/bootstrap-vue.css';>>.\src\main.js
    echo:>>.\src\main.js
    echo //TODO: Import only the components used rather than the whole library>>.\src\main.js
    echo Vue.use^(BootstrapVue^);>>.\src\main.js
    echo //TODO: Import only the icons used rather than the whole library>>.\src\main.js
    echo Vue.use^(BootstrapVueIcons^);>>.\src\main.js
    echo Vue.component^('BIconEnvelope', BIconEnvelope^);>>.\src\main.js
    echo Vue.component^('BIconTelephone', BIconTelephone^);>>.\src\main.js
    echo:>>.\src\main.js
    echo library.add^(faThumbsUp^);>>.\src\main.js
    echo Vue.component^('faicon', FontAwesomeIcon^);>>./src/main.js
    echo:>>.\src\main.js
    echo new Vue^({>>.\src\main.js
    echo     render: ^(h^) =^> h^(App^),>>.\src\main.js
    echo }^).$mount^('#app'^);>>.\src\main.js
EXIT /b

:RewriteAppVue
    echo:
    echo ===== Rewriting .\src\App.vue
    echo:
    echo ^<template^>>.\src\App.vue
    echo    ^<div id="app"^>>>.\src\App.vue
    echo        ^<Header /^>>>.\src\App.vue
    echo        ^<b-container id="content"^>>>.\src\App.vue
    echo            ^<faicon :icon="['far', 'thumbs-up']" /^>>>.\src\App.vue
    echo        ^</b-container^>>>.\src\App.vue
    echo        ^<Footer /^>>>.\src\App.vue
    echo    ^</div^>>>.\src\App.vue
    echo ^</template^>>>.\src\App.vue
    echo:>>.\src\App.vue 
    echo ^<script^>>>.\src\App.vue
    echo import Header from './components/Header.vue';>>.\src\App.vue
    echo import Footer from './components/Footer.vue';>>.\src\App.vue
    echo:>>.\src\App.vue 
    echo export default {>>.\src\App.vue
    echo    components: {>>.\src\App.vue
    echo        Header,>>.\src\App.vue
    echo        Footer>>.\src\App.vue
    echo    }>>.\src\App.vue
    echo };>>.\src\App.vue
    echo ^</script^>>>.\src\App.vue
    echo:>>.\src\App.vue 
    echo ^<style^>>>.\src\App.vue
    echo    html,>>.\src\App.vue
    echo    body {>>.\src\App.vue
    echo        height: 100%%;>>.\src\App.vue
    echo    }>>.\src\App.vue
    echo    #app {>>.\src\App.vue
    echo        height: 100%%;>>.\src\App.vue
    echo        display: flex;>>.\src\App.vue
    echo        flex-direction: column;>>.\src\App.vue
    echo        -webkit-font-smoothing: antialiased;>>.\src\App.vue
    echo        -moz-osx-font-smoothing: grayscale;>>.\src\App.vue
    echo    }>>.\src\App.vue
    echo    #content {>>.\src\App.vue
    echo        padding: 0;>>.\src\App.vue
    echo        flex: auto 1 1;>>.\src\App.vue
    echo    }>>.\src\App.vue
    echo ^</style^>>>.\src\App.vue
EXIT /b

:CreateHeaderVue
    echo:
    echo ===== Creating .\src\components\Header.vue
    echo:
    echo ^<template^>>.\src\components\Header.vue
    echo     ^<b-navbar toggleable="sm" type="light" variant="light" sticky^>>>.\src\components\Header.vue
    echo         ^<b-container^>>>.\src\components\Header.vue
    echo             ^<b-navbar-brand^>A brand^</b-navbar-brand^>>>.\src\components\Header.vue
    echo:>>.\src\components\Header.vue
    echo             ^<b-navbar-toggle target="nav-collapse"^>^</b-navbar-toggle^>>>.\src\components\Header.vue
    echo:>>.\src\components\Header.vue
    echo             ^<b-collapse id="nav-collapse" is-nav^>>>.\src\components\Header.vue
    echo                 ^<b-navbar-nav^>>>.\src\components\Header.vue
    echo                     ^<b-nav-item href="#" target="_blank"^>A link^</b-nav-item^>>>.\src\components\Header.vue
    echo                 ^</b-navbar-nav^>>>.\src\components\Header.vue
    echo:>>.\src\components\Header.vue
    echo                 ^<!-- Right aligned nav items --^>>>.\src\components\Header.vue
    echo                 ^<b-navbar-nav class="ml-auto"^>>>.\src\components\Header.vue
    echo                     ^<b-nav-item href="#"^>Another link^</b-nav-item^>>>.\src\components\Header.vue
    echo                 ^</b-navbar-nav^>>>.\src\components\Header.vue
    echo             ^</b-collapse^>>>.\src\components\Header.vue
    echo         ^</b-container^>>>.\src\components\Header.vue
    echo     ^</b-navbar^>>>.\src\components\Header.vue
    echo ^</template^>>>.\src\components\Header.vue
EXIT /b

:CreateFooterVue
    echo:
    echo ===== Creating .\src\components\Footer.vue
    echo:
    echo ^<template^>>./src/components/Footer.vue
    echo     ^<div id="footer"^>>>./src/components/Footer.vue
    echo     ^</div^>>>./src/components/Footer.vue
    echo ^</template^>>>./src/components/Footer.vue
    echo:>>./src/components/Footer.vue
    echo ^<style scoped^>>>./src/components/Footer.vue
    echo     #footer {>>./src/components/Footer.vue
    echo         flex: none;>>./src/components/Footer.vue
    echo     }>>./src/components/Footer.vue
    echo ^</style^>>>./src/components/Footer.vue
EXIT /b