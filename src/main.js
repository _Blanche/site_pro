import Vue from 'vue';
import App from './App.vue';
import { /*BootstrapVue, BootstrapVueIcons,*/
         VBToggle, BButton, BCard, BCardBody, BCardText, BCardImg, CarouselPlugin, BCollapse,
         BEmbed, BImg, LayoutPlugin, BLink, BNavItem, BNavText, BNavbar, BNavbarBrand, BNavbarNav,
         BNavbarToggle, TabsPlugin, BIconEnvelope, BIconTelephone, BIconCapslock, BIconLinkedin, BIconMastodon, BIcon } from 'bootstrap-vue';
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome';
import { library } from '@fortawesome/fontawesome-svg-core';
import { faAndroid, faApple, faGitlab, faItchIo, faLinux, faSteam, faWindows, faBluesky } from '@fortawesome/free-brands-svg-icons';
import { faExternalLinkAlt } from '@fortawesome/free-solid-svg-icons';
import { faFileCode, faFilePdf } from '@fortawesome/free-regular-svg-icons';

import 'bootstrap/dist/css/bootstrap.css';
import 'bootstrap-vue/dist/bootstrap-vue.css';

// Vue.use(BootstrapVue);
// Vue.use(BootstrapVueIcons);
Vue.use(CarouselPlugin);
Vue.use(LayoutPlugin);
Vue.use(TabsPlugin);
Vue.directive('b-toggle', VBToggle);

Vue.component('b-button', BButton);
Vue.component('b-card', BCard);
Vue.component('b-card-body', BCardBody);
Vue.component('b-card-text', BCardText);
Vue.component('b-card-img', BCardImg);
Vue.component('b-collapse', BCollapse);
Vue.component('b-embed', BEmbed);
Vue.component('b-img', BImg);
Vue.component('b-link', BLink);
Vue.component('b-nav-item', BNavItem);
Vue.component('b-nav-text', BNavText);
Vue.component('b-navbar', BNavbar);
Vue.component('b-navbar-brand', BNavbarBrand);
Vue.component('b-navbar-nav', BNavbarNav);
Vue.component('b-navbar-toggle', BNavbarToggle);

Vue.component('BIconEnvelope', BIconEnvelope);
Vue.component('BIconTelephone', BIconTelephone);
Vue.component('BIconCapslock', BIconCapslock);
Vue.component('BIconLinkedin', BIconLinkedin);
Vue.component('BIconMastodon', BIconMastodon);

Vue.component('faicon', FontAwesomeIcon);
library.add(faGitlab, faSteam, faItchIo, faWindows, faLinux, faApple, faExternalLinkAlt, faFilePdf, faAndroid, faFileCode, faBluesky);
//faFileArchive, faFileAlt
new Vue({
    render: (h) => h(App),
}).$mount('#app');
