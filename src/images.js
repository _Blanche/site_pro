const AccordionImages = import.meta.globEager("/src/assets/*");
const ProjectsImages = import.meta.globEager("/src/assets/projects/*");

export default {
    ProjectsImages,
    AccordionImages
}